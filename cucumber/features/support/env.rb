require 'rspec/expectations'
require 'selenium-webdriver'

# Where our app lives, relative to this file
APP_PATH = '../../../../../apps/TestApp/build/release-iphonesimulator/TestApp.app'


# Make sure the path above is relative to this file
def absolute_app_path
  File.join(File.dirname(__FILE__), APP_PATH)
end

# The location of our selenium (or in this case, Appium) file
def server_url
  "http://127.0.0.1:4723/wd/hub"
end
 
# Set up a driver or, if one exists, return it
def selenium
  @driver ||= Selenium::WebDriver.for(:remote, :desired_capabilities => {
      'browserName' => '',
      'platform' => 'Mac',
      'device' => 'iPhone Simulator',
      'version' => '6.1',
      'app' => 'safari'
  }, :url => server_url)
end

After { @driver.quit }
